import '../styles/globals.css'
import App from 'next/app'
import React, {useEffect, useState} from "react";
import Amplify, {Auth} from "aws-amplify";
import axios from "axios";
import {cache, SWRConfig} from "swr";
import {isBrowser, redirect, isAuthenticated, handleSignOut, getAuthBearer} from '../utils/helper';
import {ApolloProvider} from "@apollo/client";
import client from "../utils/apollo-client";
import {useRouter} from 'next/router';
import {
  RecoilRoot,
  atom,
  selector,
  useRecoilState,
  useRecoilValue,
} from "recoil";



Amplify.configure({
  Auth: {
    identityPoolId: process.env.IDENTITY_POOL_ID, //REQUIRED - Amazon Cognito Identity Pool ID
    region: process.env.USER_POOL_REGION, // REQUIRED - Amazon Cognito Region
    userPoolId: process.env.USER_POOL_ID, //OPTIONAL - Amazon Cognito User Pool ID
    userPoolWebClientId: process.env.USER_POOL_WEB_CLIENT_ID, //OPTIONAL - Amazon Cognito Web Client ID
  },
});


function MyApp({ Component, pageProps }) {
  const [group, setGroup] = useState("");

  useEffect(async () => {
    if (!group) {
      console.log("no group");
      Auth.currentAuthenticatedUser()
        .then((user) => {
          const groups = user.signInUserSession.accessToken.payload["cognito:groups"];

          // console.log('user', user);
          console.log('groups', groups);
          // save authHeaders to session storage

          const expBtkn = `Bearer ${user.signInUserSession.getIdToken().getJwtToken()}`
          window.sessionStorage.setItem('expBtkn', expBtkn);
          setGroup('MgsResearcher'); // temp

          // if (groups.find((group) => /biocore/.test(group))) {
          //   setGroup("biocore");
          // } else if (groups.find((group) => /zebra/.test(group))) {
          //   setGroup("zebra");
          // }

          console.log('group', group);
        })
        .catch((error) => console.log(error));

    } else {
      console.log("there is a group");
      axios.defaults.baseURL = process.env.NEXT_APP_API_URL;
    }
  }, [group]);

  if(isBrowser) {
    window.addEventListener("beforeunload", () => cache.clear());
  }

  const fetcher = (endpoint) => axios.get(endpoint).then((result) => result.data);

  return (
    <SWRConfig
      value={{
        fetcher,
        revalidateOnFocus: false,
        suspense: true,
      }}
    >
      <ApolloProvider client={client}>
        <RecoilRoot>
          <Component {...pageProps} />
        </RecoilRoot>
      </ApolloProvider>
    </SWRConfig>
  )
}

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext)
  return {...appProps}
}

export default MyApp
