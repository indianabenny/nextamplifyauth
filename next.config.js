module.exports = {
  env: {
    NEXT_APP_API_URL : 'https://api.dev.nfldigitalathlete.com/graphql',
    AUTH_COOKIE_DOMAIN: 'localhost',
    IDP_DOMAIN: 'da-uiux-dev-pool.auth.us-east-1.amazoncognito.com',
    USER_POOL_REGION: 'us-east-1',
    USER_POOL_ID: 'us-east-1_REqiSC5TX',
    IDENTITY_POOL_ID: 'us-east-1:6c5d5224-a58c-4e5f-8a6a-bb271948b56a',
    USER_POOL_WEB_CLIENT_ID: '2st4bphpju27qnkue5d1l9itps',
    REDIRECT_SIGN_IN: 'http://localhost:3000/',
    REDIRECT_SIGN_OUT: 'http://localhost:3000/',
  },
}