const {atom, selector} = require("recoil");

const bearerState = atom({
  key: 'bearer',
  default: '',
})

const groupState = atom({
  key: 'group',
  default: '',
})

export {
  bearerState,
  groupState,
}