import {useQuery, gql} from '@apollo/client';

const PLAY_DATA = gql`
  query Query($ngsPlayGameKey: Int!, $ngsPlayPlayId: Int!) {
    ngsPlay(gameKey: $ngsPlayGameKey, playId: $ngsPlayPlayId) {
      gameKey
      gameId
      playId
      videos(view: [Endzone, Sideline, broadcast]) {
        camera
        url
      }
      playDescription
      down
      gameClock
      playType
      events {
        name
        time
        type
      }
      players {
        nflId
        esbId
        gsisId
        displayName
        position
        positionGroup
        jerseyNumber
        team {
          teamId
          logo
          abbr
          cityState
          fullName
          nick
        }
        playerTrackingData {
          x
          y
          event
          time
        }
      } 
    }
  }
`

export default function VideoReviewer(gameKey) {
  // run the query
  const {loading, error, data} = useQuery(
    PLAY_DATA,
    {
      variables: {
        "ngsPlayGameKey": parseFloat(gameKey.gameKey),
        "ngsPlayPlayId": parseFloat(gameKey.playId)
      }
    }
  );

  if (loading) {
    return (
      <div className="h-screen">
        <div className="min-h-screen flex items-center justify-center">
          <div className="flex flex-col">
            <img src="/static/img/icn-loading.svg" alt="Loading"/>
          </div>
        </div>
      </div>
    )
  }

  if (error) {
    return (
      <div className="h-screen">
        <div className="min-h-screen flex items-center justify-center">
          <div className="flex flex-col">
            <p>Error :( {console.log(error)}</p>
          </div>
        </div>
      </div>
    )
  }

  // console.log('play data', data);
  const src = data.ngsPlay.videos[0].url;
  const players = data.ngsPlay.players;
  const otherPlayData = {
    playDescription: data.ngsPlay.playDescription,
    down: data.ngsPlay.down,
    gameClock: data.ngsPlay.gameClock,
    gameId: data.ngsPlay.gameId,
    gameKey: data.ngsPlay.gameKey,
    playId: data.ngsPlay.playId,
    playType: data.ngsPlay.playType,
    videos: data.ngsPlay.videos
  }

  return (
    <>
      <h1>Data loaded</h1>
      <div>
        {console.log(data)}
        <p>
          Game Key: {otherPlayData.gameKey}<br />
          Play ID: {otherPlayData.playId}<br/>
          Description: {otherPlayData.playDescription}</p>
        <video src={src} controls muted />
      </div>
    </>
  )

}