import Link from "next/link";
import {isAuthenticated} from "../utils/helper";
import {useEffect, useState} from "react";


export default function Nav() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(async () => {
    const isAuth = await isAuthenticated();
    if (typeof isAuth === 'object' && isAuth !== null) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  });

  return (
    <div>
      <Link href="/"><a>Home</a></Link>&nbsp;&nbsp;|&nbsp;&nbsp;
      <Link href="/test"><a>Test</a></Link>&nbsp;&nbsp;|&nbsp;&nbsp;
      <Link href="/dashboard"><a>Dashboard</a></Link>&nbsp;&nbsp;|&nbsp;&nbsp;
      {!isLoggedIn &&
      <Link href="/signin"><a>Sign In</a></Link>
      }
      {isLoggedIn &&
      <Link href="/signout"><a>Sign Out</a></Link>
      }
    </div>
  )
}