import {Auth} from "aws-amplify";
import {cache} from "swr";
import axios from "axios";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";

/**
 *
 * @type {boolean}
 */
export const isBrowser = !!(typeof window !== 'undefined' && window.document && window.document.createElement);


/**
 *
 * @param res
 * @param route
 */
export const redirect = (res, route) => {
  if (!isBrowser && res) {
    res.writeHead(307, {
      Location: route,
    });
    res.end();
  } else {
    Router.replace(route);
  }
};

/**
 * Checks for window object
 * @returns {boolean}
 */
export const hasWindow = () => {
  return typeof window === 'object'
}

if (hasWindow()) {
  // client side code
}

if (!hasWindow()) {
  // server-side code
}

/**
 * checks to see if a user is authenticated, returns object if authenticated or false if not
 * @returns {Promise<boolean|CognitoUserSession>}
 */
export const isAuthenticated = async () => {
  try {
    const userIsAuthenticated = await Auth.currentSession();
    console.log('userIsAuthenticated', userIsAuthenticated);

    const expBtkn = window.sessionStorage.getItem('expBtkn');
    if (expBtkn === null) {
      try {
        const getAuthetication = await getAuthBearer();
        // console.log('getAuthetication', getAuthetication);
      } catch( error ) {
        console.log('getAuthBearer error', error);
      }
    }

    return userIsAuthenticated;
  } catch (error) {
    console.log(error);
    return false;
  }
};

/**
 *  Signs user out of Cognito Auth
 */
export const handleSignOut = () => {
  Auth.signOut()
    .then(() => {
      cache.clear();
      setGroup("");
    })
    .catch((error) => console.log(error));
};

/**
 *
 */
export const getAuthBearer = () => {
    return new Promise((resolve) => {
      let authHeaders = false;
      Auth.currentSession()
        .then((session) => {
          // console.log('getAuthBearer session', session);

          const current = Math.floor(new Date() / 1000);
          const expiration = session.getIdToken().getExpiration();

          // console.log('getAuthBearer expiration', expiration);

          if (expiration > current) {
            // console.log('getAuthBearer expiration > current');

            Auth.currentAuthenticatedUser()
              .then((user) => {

                user.refreshSession(
                  session.getRefreshToken(),
                  (error, refreshedSession) => {

                    if (error) {
                      // console.log('error, signing out', error)
                      handleSignOut();

                    } else {

                      authHeaders = `Bearer ${refreshedSession.getIdToken().getJwtToken()}`;
                      // console.log('getAuthBearer authHeaders present');

                      // save authHeaders to session storage
                      window.sessionStorage.setItem('expBtkn', authHeaders);

                      return authHeaders;
                    }
                  }
                );
              })
              .catch((error) => {
                console.log(error);
                handleSignOut();
              });
          } else {
            // console.log('getAuthBearer expiration < current');
            authHeaders = `Bearer ${session.getIdToken().getJwtToken()}`;
            // config.headers.Authorization = authHeaders;
            // console.log('getAuthBearer authHeaders present');

            // save authHeaders to session storage
            window.sessionStorage.setItem('expBtkn', authHeaders);
            return authHeaders;
          }
          // console.log('getAuthBearer session', session);
        })
        .catch((error) => {
          // console.log('getAuthBearer error',error);
          handleSignOut();
        });
    });
}


export const setAxiosInterceptor = (authHeaders) => {
  axios.interceptors.request.use(function (config) {
    return new Promise((resolve) => {
      config.headers.Authorization = authHeaders;
      resolve(config);
    })
  });
}


export const redirectAfterAuth = async () => {
  let userIsLoggedIn = false;
  try {
    const isAuth = await isAuthenticated();
    const router = useRouter();

    console.log('isAuth', isAuth);
    if (typeof isAuth === 'object' && isAuth !== null) {
      console.log('userIsLoggedIn', userIsLoggedIn);
      await router.push('/dashboard');
    } else {
      console.log('userIsLoggedIn', userIsLoggedIn);
    }
  } catch (error) {
    console.log(error);
  }

}


