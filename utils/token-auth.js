// import {useEffect, useState} from "react";
// import {Auth} from "aws-amplify";
// import axios from "axios";
// import {useRecoilState} from "recoil";
// import {groupState} from "../components/States";
//
// export function getAuth(userAuth) {
//   console.log(userAuth);
//   const [group, setGroup] = useRecoilState(groupState)
//
//
//
//   useEffect(async () => {
//     if (!group) {
//       console.log("no group");
//       Auth.currentAuthenticatedUser()
//         .then((user) => {
//           const groups = user.signInUserSession.accessToken.payload["cognito:groups"];
//
//           // console.log('user', user);
//           console.log('groups', groups);
//           // save authHeaders to session storage
//
//           const expBtkn = `Bearer ${user.signInUserSession.getIdToken().getJwtToken()}`
//           window.sessionStorage.setItem('expBtkn', expBtkn);
//           setGroup('MgsResearcher'); // temp
//
//           // if (groups.find((group) => /biocore/.test(group))) {
//           //   setGroup("biocore");
//           // } else if (groups.find((group) => /zebra/.test(group))) {
//           //   setGroup("zebra");
//           // }
//
//           console.log('group', group);
//         })
//         .catch((error) => console.log(error));
//
//
//     } else {
//
//       console.log("there is a group");
//       axios.defaults.baseURL = process.env.NEXT_APP_API_URL;
//
//       axios.interceptors.request.use(function (config) {
//         return new Promise((resolve) => {
//           Auth.currentSession()
//             .then((session) => {
//               console.log('session', session);
//
//               const current = Math.floor(new Date() / 1000);
//               const expiration = session.getIdToken().getExpiration();
//
//
//               if (expiration > current) {
//
//                 Auth.currentAuthenticatedUser()
//                   .then((user) => {
//
//                     user.refreshSession(
//                       session.getRefreshToken(),
//                       (error, refreshedSession) => {
//
//                         if (error) {
//                           console.log('error, signing out')
//                           handleSignOut();
//
//                         } else {
//
//                           const authHeaders = `Bearer ${refreshedSession.getIdToken().getJwtToken()}`;
//                           config.headers.Authorization = authHeaders;
//                           resolve(config);
//
//                           // save authHeaders to session storage
//                           window.sessionStorage.setItem('expBtkn', authHeaders);
//                         }
//                       }
//                     );
//                   })
//                   .catch((error) => {
//                     console.log(error);
//                     handleSignOut();
//                   });
//               } else {
//                 const authHeaders = `Bearer ${session.getIdToken().getJwtToken()}`;
//                 config.headers.Authorization = authHeaders;
//
//                 // save authHeaders to session storage
//                 window.sessionStorage.setItem('expBtkn', authHeaders);
//               }
//               console.log(session);
//             })
//             .catch((error) => {
//               console.log(error);
//               handleSignOut();
//             });
//         });
//       });
//     }
//   }, [group]);
//
// }