import {ApolloClient, createHttpLink, InMemoryCache} from "@apollo/client";
import {setContext} from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: "https://api.dev.nfldigitalathlete.com/graphql"
});

const authLink = setContext((_, {headers}) => {
  // get the authentication token from session storage if it exists
  const token = window.sessionStorage.getItem('expBtkn');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  // link: httpLink,
  cache: new InMemoryCache()
});

export default client;